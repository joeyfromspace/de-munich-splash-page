/**
 * HDtracks German Landing Page JavaScript Handler
 * Created by: Joey Lappin
 */

var desktop = false;
var dmurl = { 
	windows: 'downloader/channels/v18/stable/HDtracksDownloader200032.exe',
	mac: 'downloader/channels/v18/stable/HDtracksDownloader200057.dmg'
};

$(document).ready(function() {	
	if($(window).width() > 768) {
		window.desktop = true;
	}
	// Resize padding on main viewport to fit displays
	if(window.desktop) {
		$('.main-page').valign();		
	}

	// Make sure background always covers the entire document
	$('html').observeHeight(function(oh, nh) {
		if(nh > $(window).height()) {
			$('.de-splash').children().stretch('html');
		} else {
			$('.de-splash').children().height($(window).height());
		}
	});
	$('.de-splash').children().stretch('html');	

	// Instantiate landing page object
	var landingpage = new LandingPage('form', [{ 
		name:'email',
		click: function(_self, values) {
			if(!values.email) {
				_self.drawAlert('danger', 'You must enter an email address.');
				return;
			}
			var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if(!re.test(values.email)) {
				_self.drawAlert('warning', 'You must provide a valid email address.');
				return;
			}

			_self.submitItem(values, function(err) {
				if(err) {
					_self.drawAlert('danger', 'That email address is already in use.');
					return;
				}
				_self.endCurrentPhase(function() {
					_self.beginNextPhase();
				});
			});
		}, load: function(_self) {
			$('.section-email .desktop a').attr('href', _self.getDownloadUrl());
			// Lineup TOS with bottom of cover			
			var topSet =  (($('.munich-cover').offset().top + $('.munich-text').height()) - ($('.desktop').first().offset().top)) + 21;
			$('.tos').css({ paddingTop: topSet});
			$('.tos').css({ display: 'block' }).addClass('alert-popup');
			setTimeout(function() {
				$('.tos').css({opacity: 1});
			}, 300);
		}, path: '/landing/index/checkemail'
	}, {
		name: 'password',
		click: function(_self, values) {
				// Validate password entry
				if(!values.password || !values.confirm_password) {
					_self.drawAlert('danger', 'Password and password confirmation are required.');
					return;
				}
				if(values.password.length < 6 || values.confirm_password < 6) {
					_self.drawAlert('warning', 'Password must be at least 6 characters.');
					return;
				}
				if(values.password != values.confirm_password) {
					_self.drawAlert('danger', 'Passwords do not match.');
					return;
				}

				// Format submit items as the server-side script expects to see them.
				if(_self.submittedData.hasOwnProperty('email')) {
					values.landing_email = _self.submittedData.email;
				}
				
				values['landing_password'] = values.password;
				if(values.itunes) {
					values['audioplayer'] = 25;
				} else {
					values['audioplayer'] = 23;
				}

				_self.submitItem(values, function(err) {
					if(err) {
						_self.drawAlert('danger', err);
						return;
					}

					// Set cookie to true for this site
					_self.setCookie('HDTRACKSDM', 1, 30);								

					_self.endCurrentPhase(function() {
						_self.beginNextPhase();
					});
				});
		}, path: '/landing/index/post' 
	}, {		
		name: 'dm-load',
		load: function(_self) {
			$('.dm-load').addClass('dm-load-animation');
			setTimeout(function() {
				_self.endCurrentPhase(function() {
					_self.beginNextPhase(function() {												
					});
				});
			}, 5000);
		}
	}, {
		name: 'dm',
		load: function(_self) {
			
			var url = _self.getDownloadUrl();

			if(url) {
				$('a.btn-info').attr('href', url);
			} else {
				$('.desktop').addClass('alert-popout');
				$('.no-mobile').removeClass('hidden').addClass('alert-popup');
				setTimeout(function() {
					$('.desktop').remove();
					$('.no-mobile').css({ opacity: 1 });
				}, 300);
			}			
		}
	}], { delay: 300, serverOverride: 'http://localhost/post-test/', domain: 'stagenew.german.hdtracks.com', dmurl: dmurl });	
});

$(window).resize(function() {
	if(window.desktop) {
		$('.main-page').valign();
	}
	$('.de-splash').children().stretch('html');
});


// Custom jQuery methods
$.fn.extend({
	valign: function() {
		var padding = $(window).height() / 3;
		$(this).css({paddingTop: padding});

		return this;
	}, stretch: function(selector) {
		// Requires selector to stretch to
		if(typeof(selector) == 'undefined') {
			return false;
		}

		if($(this).height() < $(selector).prop('scrollHeight')) {
			$(this).css({height: $(selector).prop('scrollHeight')});
		}
		return this;
	}, observeHeight: function(callback) {
		var _this = this;
		var oh = $(this).prop('scrollHeight');
		var observer = setInterval(function() {
			var ch = $(_this).prop('scrollHeight');
			if(ch != oh) {
				callback(oh, ch);
				oh == ch;
			}
		},200);
	}
});

// Prototype for handling landing page functionality
var LandingPage = function(scopeSelector, sequence, options) {	
	if(typeof(options) == 'undefined') options = {};
	if(typeof(sequence) == 'undefined') sequence = [];

	var landingPageObject = {
		init: function() {
			var _self = this;

			// Initialize object variables
			_self.options = options;
			_self.sequence = sequence;
			_self.isComplete = _self.getCookie('HDTRACKSDM');

			// If complete cookie is set, start at dm phase...
			if(_self.isComplete) {
				var dmLoadLocation;
				for(var i = 0; i < _self.sequence.length; i++) {
					if(_self.sequence[i].hasOwnProperty('name') && _self.sequence[i].name == 'dm-load') {
						dmLoadLocation = i;
					}
				}
				_self.sequence = _self.sequence.crop(dmLoadLocation);				
			}

			// Set action sequence to first item in sequence array
			_self.beginNextPhase();

			
			// Add handler for alert styles
			_self.setAlertStyles();

			// Initialize event listener
			$(_self.scope).find('button').on('click', function(e) {
				e.preventDefault();
				// Do not continue if animation is running.
				if(_self.animating) return;
				_self.handleClick(e);
			});

			// Fix for webkit messing up animations
			$(".alert-inner").on("webkitTransitionEnd", function () {
    			$(this).hide().offset();
    			$(this).show();
			});
		}, handleClick: function(clickEvent) {			

				var _self = this;
								
				// Sanitize input
				_self.sanitizeInput(function(err, values) {
					if(_self.sequencePhase.hasOwnProperty('click')) {
						_self.sequencePhase.click(_self, values);
					}

				});
		}, submitItem: function(values, callback) {
			var _self = this, url;

			// Use hard-coded URL if available
			if(_self.options.serverOverride) {
				url = _self.options.serverOverride
			} else if(_self.sequencePhase.hasOwnProperty('path')) {
				url = 'https://'+_self.options.domain+_self.sequencePhase.path;
			}

			$.ajax({
				type: 'post',
				url: url,
				dataType: 'json',
				data: values,
				complete: function(response) {
					if(response) {
						for(property in values) {
							if(values.hasOwnProperty(property)) {
								_self.submittedData[property] = values[property];
							}
						}						
						return callback(false);
					} else {
						return callback('Request failed.');
					}
				}
			});
		}, drawAlert: function(type, text, delay) {
			if(typeof(delay) == 'undefined') delay = this.options.delay || 300;
			var rando = randomRange(10000, 99999),
				alert = new Alert(type, text, delay);
			alert.drawAlert(function() {
				alert.eraseAlert();
			});
		}, changeSequencePhase: function(options, callback) {	
			var _self = this;	
			if(_self.animating) return;
			_self.animating = true;

			if(typeof(options) == 'string') {
				options = { stage: options };
			}

			if(typeof(options) == 'function') {
				callback = options;				
			}

			var animations = $('.section-'+this.sequencePhase.name).children(),
				running = false,
				ran = 0,				
				delay = options.delay || this.options.delay || 300,
				stage = options.stage || 'end',
				stageClass;						
			// Set animation-out classes for the selected stage
			stageClass = stage == 'start' ? 'alert-popup' : 'alert-popout';
			if(stage == 'start') $('.section-'+this.sequencePhase.name).removeClass('hidden').children().css({ opacity: 0 });

			// Do the animation
			var doAnim = function($item) {
				running = true;
				$item.addClass(stageClass);
				setTimeout(function() {
					running = false;
					++ran;
					if(stage == "start") {
						$item.css({ opacity: 1 });
					} else {
						$item.css({ 'visibility': 'hidden', opacity: 0 });
					}
				}, delay);
			};

			// Check for animation's turn
			var checkAnim = function($item) {
				if($item.css('display') != 'none') {
					var checkNow = setInterval(function() {
						if(running == false) {					
							clearInterval(checkNow);
							doAnim($item);
						}
					},10);
				} else {
					ran++;
				}
			};

			$(animations).each(function() {
				checkAnim($(this));
			});

			// Check for queued animations completion and return callback
			var checkDone = setInterval(function() {
				if(ran == animations.length) {
					clearInterval(checkDone);
					_self.animating = false;
					return callback();
				}
			}, 10);

		}, endCurrentPhase: function(delay, callback) {
			if(typeof(delay) == 'function') {
				callback = delay;
				delay = this.options.delay || 300;
			}

			var _self = this;

			_self.changeSequencePhase({ delay: delay, stage: 'end' }, function() {
				$('.section-'+_self.sequencePhase.name).remove();
				return callback();
			});						
		}, beginNextPhase: function(options, callback) {			
			if(typeof(options) == 'number' || typeof(options) == 'string') {
				options = { delay: options };								
			}
			if(typeof(options) == 'function') {
				callback = options;
				options = { delay: 300 };
			}
			if(typeof(callback) == 'undefined') {
				callback = function() {};
			}
			if(typeof(options) == 'undefined') {
				options = {};				
			}

			var delay = options.delay || this.options.delay || 300;

			var _self = this;
			_self.sequencePhase = _self.sequence.shift();

			if(!_self.sequencePhase) {
				_self.finishSequence();
				return callback();
			}

			_self.changeSequencePhase('start', function() {
				if(_self.sequencePhase.hasOwnProperty('load')) {
					_self.sequencePhase.load(_self);
				}
				return callback();
			});

		}, finishSequence: function() {
			
			console.log('All done!');

		}, sanitizeInput: function(callback) {
			var values = {},_self = this;

			async.series([function(next) {
				async.each($(_self.scope+' .section-'+_self.sequencePhase.name+' input'), function(input, callback) {
					if($(input).attr('type') == 'checkbox' && $(input).prop('checked')) {
						values[$(input).attr('name')] = true;
					} else if ($(input).attr('type') == 'checkbox') {		
						values[$(input).attr('name')] = false;
					} else if(!$(input).val()) {
						values[$(input).attr('name')] = $('input[name="'+$(input).attr('name')+'-mobile"]').val();
					} else {
						values[$(input).attr('name')] = $(input).val();					
					}
					callback();
				}, function(err) {
					next();
				});		
			}, function(next) {
				var length = Object.size(values), i = 0;
				for (var property in values) {
					if(values.hasOwnProperty(property)) {
						if(property.indexOf('-mobile') != -1) {							
							delete values[property];
						}
						i++;
					}
				}				
				var testy = setInterval(function() {
					if(i == length) {
						clearInterval(testy);
						next();
					}
				},10);
			}], function() {
				return callback(null, values);
			});
		}, getCookie: function(cookie) {
			//getCookie function lifted from original landing page code

			var x,y,ARRcookies = document.cookie.split(';');

			for(var i =0; i < ARRcookies.length; i++) {
				x = ARRcookies[i].substr(0, ARRcookies[i].indexOf('='));
				y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);

				x = x.replace(/^\s+|\s+$/g,"");

				if(x == cookie) {
					return unescape(y);
				}
			}

			return false;

		}, setCookie: function(cookieName, value, expiryDays) {
			// setCookie function lifted from original landing page code
			var expiry = new Date();
			expiry.setDate(expiry.getDate() + expiryDays);
			var cookieValue = escape(value) + ((expiryDays == null) ? "" : "; expires="+expiry.toUTCString());
			
			document.cookie = cookieName + "=" + cookieValue;

		}, setAlertStyles: function() {
			var count = 101,headstyles = "<style>",done=false;
			for (var i = 1; i <= count; i++) {
				if(i == count) {
					done = true;
				}
				if(!done) { 					
					headstyles = headstyles.concat('.alert-container .alert-wrap:nth-child('+i+') { transform:translate3d(0, '+(i - 1)+'00%, 0);}');
				}
			};
			var checkLoop = setInterval(function() {
				if(done) {
					clearInterval(checkLoop);
					$('head').append(headstyles+'\n</style>');
				}
			},10);		
		}, getDownloadUrl: function() {
			var url = 'http://' + window.location.host + '/',
				plat = navigator.platform.toUpperCase();

			if(plat.indexOf('MAC') != -1) {
				return url.concat(this.dmurl.mac);
			} else if(plat.indexOf('WIN32') != -1) {
				return url.concat(this.dmurl.windows);
			} else {
				return false;
			}
		}, submittedData: {}, options: options, scope: scopeSelector, dmurl: options.dmurl || null, sequence: [], sequencePhase: null, animating: false
	}
	
	landingPageObject.init();

	return landingPageObject;
}

var Alert = function(type, text, delay) {
	var seed = randomRange(10000, 99999);

	var alert = {
		type: type,
		text: text,
		delay: delay,
		seed: seed,
		drawAlert: function(callback) {
			var _self = this;
			var $domContent = $('<div class="alert-wrap"><div class="alert alert-'+type+' alert-'+_self.seed+' hidden" role="alert">'+text+'</div></div>');
			if($('.alert-inner .alert').length) {
				$domContent.insertBefore($('.alert-inner').children().first());		
			} else {
				$domContent.appendTo('.alert-inner');
			}
			setTimeout(function() {
				$domContent.find('.alert-'+_self.seed).removeClass('hidden').addClass('alert-popup');
				setTimeout(function() {
					$domContent.find('.alert-'+_self.seed).removeClass('alert-popup');
					setTimeout(function() {
						$domContent.find('.alert-'+_self.seed).addClass('alert-popout');
						setTimeout(function() {
							$domContent.remove();
						}, _self.delay);
					},5000);
				},_self.delay);
			}, _self.delay);
		}
	}

	return alert;
}
randomRange = function(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

Object.size = function(obj) { 
	var size = 0, key;
	for(key in obj) {
		if(obj.hasOwnProperty(key)) size++;
	}
	return size;
};

Array.prototype.crop = function(endIndex) {
	this.concat(this.splice(0,endIndex));
	return this;
}